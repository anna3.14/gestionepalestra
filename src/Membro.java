import java.time.LocalDate;

/**
 * Classe membro: è la classe padre di tutti gli utenti della palestra.
 * Presenta metodi getter e setter e un costruttore.
 */
public class Membro {
    private String nome;
    private String cognome;
    private String IDMembro;
    private TipoAbbonamento abbonamento;
    private LocalDate myDate;

    /**
     * Metodo get per ottenere nome membro
     * @return String
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo set per definire il nome del membro
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo get per ottenere cognome membro
     * @return String
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * Metodo set per definire cognome membro
     * @param cognome
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * Metodo get per ottenere ID membro
     * @return String
     */
    public String getIDMembro() {
        return IDMembro;
    }

    /**
     * Metodo set per definire ID membro
     * @param IDMembro
     */
    public void setIDMembro(String IDMembro) {
        this.IDMembro = IDMembro;
    }


    /**
     * Metodo get per ottenere Abbonamento membro
     * @return TipoAbbonamento
     */
    public TipoAbbonamento getAbbonamento() {
        return abbonamento;
    }

    /**
     * Metodo set per per definire ID membro
     * @param abbonamento
     */
    public void setAbbonamento(TipoAbbonamento abbonamento) {
        this.abbonamento = abbonamento;
    }

    /**
     * Metodo get per ottenere data abbonamento
     * @return LocalDate
     */
    public LocalDate getMyDate() {
        return myDate;
    }

    /**
     * Metodo set per settare data abbonamento
     * @param myDate
     */
    public void setMyDate(LocalDate myDate) {
        this.myDate = myDate;
    }

    /**
     * Costruttore Membro
     * @param nome
     * @param cognome
     */
    //Costruttore
    public Membro(String nome, String cognome){
        this.nome=nome;
        this.cognome=cognome;
        this.IDMembro=null;
        this.abbonamento=null;
        LocalDate myDate = LocalDate.now();
        this.myDate=null;
    }


}
