/**
 * Eccezione personalizzata ClassePienaException per i casi in cui il numero di iscritti supera i limiti previsti.
 * Estende Exception e prevede un costruttore con messaggio personalizzato.
 */
public class ClassePienaException extends Exception {
    /**
     * Costruttore con messaggio personalizzato
     * @param message
     */
    public ClassePienaException(String message){
        super(message);
    }

}
