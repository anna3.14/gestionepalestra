import java.util.ArrayList;
import java.util.List;

/**
 * La classe ClasseDiAllenamento rappresenta un corso della palestra. Ha tutti i metodi getter e setter, il
 * costruttore con parametri e un metodo addMembro per aggiungere membri al corso.
 */
public class ClasseDiAllenamento {
    private String nomeClasse;
    private String orario; //Format obbligatorio: lun:7-13
    private List<Membro> listaMembri;
    private Istruttore istruttoreAssegnato;
    private int numeroMaxIscritti;
    List<Integer> valutazioni;
    private int numeroIscritti;

    /**
     * Costruttore per la ClasseDiAllenamento con parametri.
     * @param nomeClasse
     * @param orario
     * @param istruttoreAssegnato
     * @param numeroMaxIscritti
     */
    //Costruttore
    public ClasseDiAllenamento(String nomeClasse, String orario, Istruttore istruttoreAssegnato, int numeroMaxIscritti ){
        this.nomeClasse=nomeClasse;
        this.orario=orario;
        this.istruttoreAssegnato=istruttoreAssegnato;
        this.numeroMaxIscritti=numeroMaxIscritti;
        List<Membro> listaMembri=new ArrayList<>();
        this.listaMembri=listaMembri;
        List<Integer> valutazioni=new ArrayList<>();
        this.valutazioni=valutazioni;
    }


    /**
     * Metodo get per ottenere il nome della classe
     * @return String
     */
    //Metodi get e set
    public String getNomeClasse() {
        return nomeClasse;
    }

    /**
     * Metodo set per settare il nome della classe
     * @param nomeClasse
     */
    public void setNomeClasse(String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }

    /**
     * Metodo get per ottenere l'orario della classe
     * @return String
     */
    public String getOrario() {
        return orario;
    }

    /**
     * Metodo set per settare l'orario della classe
     * @param orario
     */
    public void setOrario(String orario) {
        this.orario = orario;
    }

    /**
     * Metodo get per ottenere listaMembri
     * @return List<Membro>
     */
    public List<Membro> getListaMembri() {
        return listaMembri;
    }

    /**
     * Metodo set per settare listaMembri
     * @param listaMembri
     */
    public void setListaMembri(List<Membro> listaMembri) {
        this.listaMembri = listaMembri;
    }

    /**
     * Metodo get per ottenere l'istruttore assegnato al corso.
     * @return Istruttore
     */
    public Istruttore getIstruttoreAssegnato() {
        return istruttoreAssegnato;
    }

    /**
     * Metodo set per settare l'istruttore del corso
     * @param istruttoreAssegnato
     */
    public void setIstruttoreAssegnato(Istruttore istruttoreAssegnato) {
        this.istruttoreAssegnato = istruttoreAssegnato;
    }

    /**
     * Metodo get per ottenere il numero massimo di iscritti per un corso
     * @return int
     */
    public int getNumeroMaxIscritti() {
        return numeroMaxIscritti;
    }

    /**
     * Metodo set per settare il numero massimo di iscritti per un corso
     * @param numeroMaxIscritti
     */
    public void setNumeroMaxIscritti(int numeroMaxIscritti) {
        this.numeroMaxIscritti = numeroMaxIscritti;
    }

    /**
     * Metodo get per ottenere il numero corrente di iscritti per un corso
     * @return int
     */
    public int getNumeroIscritti() {
        return numeroIscritti;
    }

    /**
     * Metodo set per settare il numero corrente di iscritti per un corso
     * @param numeroIscritti
     */
    public void setNumeroIscritti(int numeroIscritti) {
        this.numeroIscritti = numeroIscritti;
    }

    /**
     * Metodo get per ottenere le valutazioni degli utenti sul corso.
     * @return List<Integer>
     */
    public List<Integer> getValutazioni() {
        return valutazioni;
    }

    /**
     * Metodo set per settare le valutazioni degli utenti sul corso.
     * @param voto
     */
    public void setValutazioni(int voto) {
        valutazioni.add(voto);
        this.valutazioni = valutazioni;
    }

    /**
     * Metodo per aggiungere un nuovo membro al corso. Il metodo allunga la lista membri e incrementa il numero di iscritti.
     * @param membro
     */
    //Add membro
    public void addMembro(Membro membro){
        this.listaMembri.add(membro);
        numeroIscritti++;
    }
}
