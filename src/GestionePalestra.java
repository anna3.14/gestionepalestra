import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe principale del programma. Gestione palestra offre la possibilità di aggiungere utenti (anche istruttori),
 * aggiungere corsi, iscrivere membri ai corsi, visualizzare la timetable e le recensioni di ogni singola classe.
 */
public class GestionePalestra {
    Map<String,TipoAbbonamento> abbonamentiMembri;
    List<ClasseDiAllenamento> classiDisponibiliPalestra;
    Map<ClasseDiAllenamento,List<Membro>> listaIscrittiSingoliCorsi;
    Map<String,ClasseDiAllenamento> timetableCorsi; //String è l'orario
    Map<String,Float> mediaValutazioni; //String nome corso e media valutazioni
    static int contatoreID;

    //Metodi get e set

    /**
     * Metodo get per ritornare la hashMap abbonamentoMembri avente come key l'ID del membro e come value il TipoAbbonamento.
     * @return Map<String,TipoAbbonamento>
     */
    public Map<String,TipoAbbonamento> getAbbonamentiMembri(){
        return abbonamentiMembri;
    }

    /**
     * Metodo get per ritornare la lista con le classi di allenamento.
     * @return List<ClasseDiAllenamento>
     */
    public List<ClasseDiAllenamento> getClassiDisponibiliPalestra(){
        return classiDisponibiliPalestra;
    }

    /**
     * Metodo get per ritornare la hashMap avente come key la classe di allenamento e come value la lista degli iscritti a quella classe.
     * @return Map<ClasseDiAllenamento,List<Membro>>
     */
    public Map<ClasseDiAllenamento,List<Membro>> getListaIscrittiSingoliCorsi(){
        return listaIscrittiSingoliCorsi;
    }

    /**
     * Metodo get per ritornare la timetable avente come key l'orario del corso e come value la classe stessa di allenamento.
     * @return Map<String,ClasseDiAllenamento>
     */
    public Map<String,ClasseDiAllenamento> getTimetableCorsi(){
        return timetableCorsi;
    }

    /**
     * Metodo get per ritornare la hashMap con nome del corso(key) e media delle valutazioni (value).
     * @return Map<String,Float>
     */
    public Map<String,Float> getMediaValutazioni(){
        return mediaValutazioni;
    }


    /**
     * Costruttore senza parametri della classe GestionePalestra
     */
    //Costruttore
    public GestionePalestra(){
        Map<String,TipoAbbonamento> abbonamentiMembri=new HashMap<>();
        this.abbonamentiMembri=abbonamentiMembri;
        List<ClasseDiAllenamento> classiDisponibiliPalestra=new ArrayList<>();
        this.classiDisponibiliPalestra=classiDisponibiliPalestra;
        Map<ClasseDiAllenamento,List<Membro>> listaIscrittiSingoliCorsi=new HashMap<>();
        this.listaIscrittiSingoliCorsi=listaIscrittiSingoliCorsi;
        Map<String,ClasseDiAllenamento> timetableCorsi=new HashMap<>();
        this.timetableCorsi=timetableCorsi;
        Map<String,Float> mediaValutazioni=new HashMap<>();
        this.mediaValutazioni=mediaValutazioni;

    }

    /**
     * Metodo per gestire la registrazione di nuovi membri e la definizione di abbonamento.
     * @param membro
     * @param abbonamento
     */
    //Registrazione di nuovi membri e definizione abbonamento
    public void registrazioneUtenti(Membro membro, TipoAbbonamento abbonamento){
        contatoreID++;
        membro.setIDMembro(Integer.toString(contatoreID));
        membro.setAbbonamento(abbonamento);
        abbonamentiMembri.put(membro.getIDMembro(), abbonamento);
        membro.setMyDate(LocalDate.now());
    }

    /**
     * Metodo per aggiungere nuove classi alla palestra con conseguente aggiornamento
     * della timetable e creazione della hashMap per le valutazioni
     * @param newClasse
     */
    //Aggiunta di nuove classi di allenamento
    public void aggiuntaClassi(ClasseDiAllenamento newClasse){
        classiDisponibiliPalestra.add(newClasse);
        timetableCorsi.put(newClasse.getOrario(),newClasse);
        mediaValutazioni.put(newClasse.getNomeClasse(),null);
    }

    //Iscrizione dei membri alle classi di allenamento

    /**
     * Metodo per iscrivere un membro ad un corso offerto dalla palestra. Ritorna eccezioni se si è raggiunto
     * il limite di capienza o se l'abbonamento del membro è scaduto.
     * @param membro
     * @param classe
     * @throws ClassePienaException
     * @throws AbbonamentoNonValidoException
     */
    public void iscrizioneMembriClassi(Membro membro, ClasseDiAllenamento classe) throws ClassePienaException,AbbonamentoNonValidoException {
        if (classe.getNumeroIscritti() > classe.getNumeroMaxIscritti()) {
            throw new ClassePienaException("Raggiunto limite per questa classe");
        }
        LocalDate myObj = LocalDate.now();
        int anni = myObj.getYear() - membro.getMyDate().getYear();
        int mesi = myObj.getMonthValue() - membro.getMyDate().getMonthValue();
        int giorni = myObj.getDayOfMonth() - membro.getMyDate().getDayOfMonth();
        int periodo = (anni * 365) + (mesi * 12) + giorni;
        //System.out.println(periodo);
        switch (membro.getAbbonamento()) {
            case giornaliero:
                if (periodo > 1) {
                    throw new AbbonamentoNonValidoException("Scaduto");
                }
                break;
            case settimanale:
                if (periodo > 7) {
                    throw new AbbonamentoNonValidoException("Scaduto");
                }
                break;
            case mensile:
                if (periodo > 31) {
                    throw new AbbonamentoNonValidoException("Scaduto");
                }
                break;
        }

        classe.addMembro(membro);
        listaIscrittiSingoliCorsi.put(classe, classe.getListaMembri());
    }

    /**
     * Metodo per visualizzare la timetable della palestra (molto primitivo)
     */
    //Metodo per visualizzare il timetable (MOLTO MOLTO PRIMITIVO)
    public void stampaTimetablePalestra(){
        for(Map.Entry<String,ClasseDiAllenamento>entry : timetableCorsi.entrySet()){
            System.out.println(entry.getKey()+"-->"+entry.getValue().getNomeClasse());
            System.out.println("Istruttore: "+entry.getValue().getIstruttoreAssegnato().getNome());
            System.out.println("Numero di posti disponibili: "+entry.getValue().getNumeroIscritti()+"/"+entry.getValue().getNumeroMaxIscritti());
        }
    }

    /**
     * Metodo per recensire una classe di allenamento a cui il membro deve aver preso precedentemente parte.
     * Aggiorna la lista di valutazione della singola classe di allenamento.
     * @param classe
     * @param membro
     * @param voto
     */
    //Metodo per recensioni
    public void recensireClassi(ClasseDiAllenamento classe, Membro membro, int voto) {
        //Solo se il membro ha partecipato

        if(classe.getListaMembri().contains(membro)) {
            System.out.println("Puoi recensire!");
            classe.getValutazioni().add(voto);
        }
        else{
            System.out.println("Non ha seguito il corso");
        }

    }

    /**
     * Metodo che ritorna la valutazione media di una classe di allenamento a seguito delle recensioni dei membri.
     * @param classe
     * @return
     */
    //Ritorna media valutazioni 1 classe
    public float ottieniRecensioneMedia(ClasseDiAllenamento classe){
        float somma=0;
        for(Map.Entry<String,Float> entry:mediaValutazioni.entrySet() ){
            if(entry.getKey().equals(classe.getNomeClasse())){
                for(int i=0;i<classe.getValutazioni().size();i++)
                    somma=somma+classe.getValutazioni().get(i);
            }
        }
        somma=somma/classe.getValutazioni().size();
        return somma;
    }

}
