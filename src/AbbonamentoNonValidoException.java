/**
 * Eccezione personalizzata AbbonamentoNonValidoException che estende Exception. Viene usata per dichiarare quando
 * un abbonamento è scaduto e non è possibile iscriversi ai corsi.
 */
public class AbbonamentoNonValidoException extends Exception{
    /**
     * Costruttore con messaggio personalizzato.
     * @param message
     */
    public AbbonamentoNonValidoException(String message){
        super(message);
    }
}
