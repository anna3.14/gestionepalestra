import java.util.Map;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws Exception {
        GestionePalestra myWarrior=new GestionePalestra();
        Istruttore istruttore1=new Istruttore("Andres", "Biffi", 3, "funzionale");
        Istruttore istruttore2=new Istruttore("Laura","Brambilla",10,"cardio");
        ClasseDiAllenamento functional=new ClasseDiAllenamento("Functional Fitness", "lun:8-9", istruttore1, 5);
        ClasseDiAllenamento cardio=new ClasseDiAllenamento("Cardio", "lun:11-12", istruttore2, 7);

        Membro anna=new Membro("Anna","Crippa");
        Membro mattia=new Membro("Mattia", "Consonni");
        Membro pio=new Membro("Pio", "D'Anna");
        Membro fabrizio=new Membro("Fabrizio","Pesce");

        //Registrazione utenti (sia istruttori che atleti)
        myWarrior.registrazioneUtenti(anna,TipoAbbonamento.mensile);
        myWarrior.registrazioneUtenti(mattia,TipoAbbonamento.mensile);
        myWarrior.registrazioneUtenti(pio,TipoAbbonamento.settimanale);
        myWarrior.registrazioneUtenti(fabrizio,TipoAbbonamento.giornaliero);
        myWarrior.registrazioneUtenti(istruttore1,TipoAbbonamento.istruttore);
        myWarrior.registrazioneUtenti(istruttore2,TipoAbbonamento.istruttore);

        //registrazione corsi
        myWarrior.aggiuntaClassi(functional);
        myWarrior.aggiuntaClassi(cardio);

        //Iscrizione dei membri alle classi di allenamento
        try {
            myWarrior.iscrizioneMembriClassi(anna, functional);
        }catch(ClassePienaException e){
            System.out.println(e.getMessage());
        }catch(AbbonamentoNonValidoException ex){
            System.out.println(ex.getMessage());
        }
        try {
            myWarrior.iscrizioneMembriClassi(fabrizio, functional);
        }catch(ClassePienaException e){
            System.out.println(e.getMessage());
        }catch(AbbonamentoNonValidoException ex){
            System.out.println(ex.getMessage());
        }
        try {
            myWarrior.iscrizioneMembriClassi(anna, cardio);
        }catch(ClassePienaException e){
            System.out.println(e.getMessage());
        }catch(AbbonamentoNonValidoException ex){
            System.out.println(ex.getMessage());
        }
        try {
            myWarrior.iscrizioneMembriClassi(pio, cardio);
        }catch(ClassePienaException e){
            System.out.println(e.getMessage());
        }catch(AbbonamentoNonValidoException ex){
            System.out.println(ex.getMessage());
        }
        try {
            myWarrior.iscrizioneMembriClassi(pio, functional);
        }catch(ClassePienaException e){
            System.out.println(e.getMessage());
        }catch(AbbonamentoNonValidoException ex){
            System.out.println(ex.getMessage());
        }

        // Stampa lista iscritti e tipologie abbonamento
        System.out.println("Stampa coppie ID-tipo abbonamento: ");
        System.out.println(myWarrior.getAbbonamentiMembri());

        System.out.println("--------------------------------------------------");
        //Stampa corsi disponibili della palestra
        System.out.println("Classi disponibili nella palestra myWarrior: ");
        for(int i=0;i<myWarrior.getClassiDisponibiliPalestra().size();i++){
            ClasseDiAllenamento classe=myWarrior.getClassiDisponibiliPalestra().get(i);
            System.out.println(classe.getNomeClasse());
        }
        System.out.println("---------------------------------------------------");
        //Stampa timetable della palestra con giorni/orari e posti disponibili
        System.out.println("Timetable: ");
        myWarrior.stampaTimetablePalestra();


        System.out.println("----------------------------------------------------");
        //Gli utenti recensiscono le classi
        myWarrior.recensireClassi(cardio,anna,7);
        myWarrior.recensireClassi(cardio,mattia,6);
        myWarrior.recensireClassi(functional,pio,10);
        myWarrior.recensireClassi(cardio,pio,3);

        //Stampa valutazioni medie per corso

        float media=myWarrior.ottieniRecensioneMedia(cardio);
        System.out.println(cardio.getValutazioni());
        System.out.println("Valutazione media cardio: "+media);


        float media1 = myWarrior.ottieniRecensioneMedia(functional);
        System.out.println(functional.getValutazioni());
        System.out.println("Valutazione media functional: "+media1);



    }
}