/**
 * TipoAbbonamento mi dà le possibili tipologie di abbonamento per i membri.
 */
public enum TipoAbbonamento {
    giornaliero,
    mensile,
    settimanale,
    istruttore,
    scaduto
}
