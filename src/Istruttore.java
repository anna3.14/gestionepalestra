/**
 * Classe Istruttore che rappresenta gli istruttori della palestra. Ci sono metodi getter e setter e un costruttore con parametri.
 */
public class Istruttore extends Membro {
    private String specializzazione;
    private int anniEsperienza;


    /**
     * Metodo get per ottenere la specializzazione dell'istruttore
     * @return String
     */
    public String getSpecializzazione() {
        return specializzazione;
    }

    /**
     * Metodo set per settare la specializzazione dell'istruttore
     * @param specializzazione
     */
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    /**
     * Metodo get per ottenere gli anni di esperienza dell'istruttore
     * @return int
     */
    public int getAnniEsperienza() {
        return anniEsperienza;
    }

    /**
     * Metodo set per settare gli anni di esperienza dell'istruttore
     * @param anniEsperienza
     */
    public void setAnniEsperienza(int anniEsperienza) {
        anniEsperienza = anniEsperienza;
    }

    /**
     * Costruttore con parametri
     * @param nome
     * @param cognome
     * @param anniEsperienza
     * @param specializzazione
     */
    //Costruttore
    public Istruttore(String nome, String cognome, int anniEsperienza, String specializzazione){
        super(nome,cognome);
        this.anniEsperienza=anniEsperienza;
        this.specializzazione=specializzazione;
    }
}
